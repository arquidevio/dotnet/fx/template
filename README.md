# fx-template

Arquidev's Framework Extensions Solution Template.

[![NuGet Badge](https://buildstats.info/nuget/Arquidev.Fx.Template?includePreReleases=true)](https://www.nuget.org/packages/Arquidev.Fx.Template)

## Usage

```sh
dotnet new arq-sln --component db/migrations --description 'SQL Database Migrations powered by DbUp' --name Arquidev.Fx.Db.Migrations
```

2023 - 2024 © [Arquidev](https://arquidev.com)
