module Arquidev.Fx.Template.Tests

open Arquidev.Fx.Template.Core
open Expecto

[<Tests>]
let tests =
    testList "fix me" [ testTask "fix me" { "Fix me" |> Expect.equal true false } ]
